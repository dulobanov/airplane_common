from setuptools import setup, find_packages

setup(
    name="airplane-common",
    version="0.0.1",
    packages=find_packages(),
    install_requires=[
        "pika",
    ],
)
