from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum

# Constants

VHOST = "/"


################################################################################


class BaseRMQOptions(ABC):
    @property
    @abstractmethod
    def create_options(self) -> dict:
        pass


################################################################################

# Exchange

class ExchangeType(Enum):
    DIRECT = "direct"


@dataclass(frozen=True)
class Exchange(BaseRMQOptions):
    name: str = "{username}-exchange"
    exchange_type: ExchangeType = ExchangeType.DIRECT
    durable: bool = True

    def __init__(self, username):
        object.__setattr__(self, "name", self.__class__.name.format(username=username))

    @property
    def create_options(self) -> dict:
        return {
            "name": self.name,
            "exchange_type": self.exchange_type.value,
            "durable": self.durable,
        }


################################################################################

# Queues

class BaseQueue(BaseRMQOptions):
    name: str
    _routing_key: Enum
    durable: bool = True
    arguments: dict = {}

    @property
    def routing_keys(self) -> list:
        return [en.value for en in list(self._routing_key)]

    @property
    def create_options(self) -> dict:
        return {
            "name": self.name,
            "routing_keys": self.routing_keys,
            "arguments": self.arguments,
            "durable": self.durable,
        }


# Airplane Queue

class PilotRMQMessageRoutingKeys(Enum):
    AIRPLANE_COMMAND = "airplane_command"


@dataclass(frozen=True)
class AirplaneQueue(BaseQueue):
    name: str = "{username}-to-airplane"

    def __init__(self, username):
        object.__setattr__(self, "name", self.__class__.name.format(username=username))
        object.__setattr__(self, "_routing_key", PilotRMQMessageRoutingKeys)
        object.__setattr__(self, "arguments", {"x-max-length": 1})


########################################

# Pilot Queue


class AirplaneRMQMessageRoutingKeys(Enum):
    STATE = "state"
    CPU = "cpu"
    FRONT_CAMERA = "front_camera"
    PING = "ping"


@dataclass(frozen=True)
class PilotQueue(BaseQueue):
    name: str = "{username}-to-pilot"

    def __init__(self, username):
        object.__setattr__(self, "name", self.__class__.name.format(username=username))
        object.__setattr__(self, "_routing_key", AirplaneRMQMessageRoutingKeys)
        object.__setattr__(self, "arguments", {"x-max-length": 100})
