import logging
import pika
import ssl

logger = logging.getLogger(__name__)


class RMQConnection:

    def __init__(self, host, port, username, password, ca_file=None, ca_data=None):
        super().__init__()
        self._ca_file = ca_file
        self._ca_data = ca_data
        self._connection = None
        self._channel = None
        self._host = host
        self._port = port
        self._username = username
        self._password = password

    def _get_connection(self):
        """
        Return connection to RabbitMQ
        """
        ssl_options = None
        if self._ca_file or self._ca_data:
            ssl_options = pika.SSLOptions(
                context=ssl.create_default_context(cafile=self._ca_file, cadata=self._ca_data),
                server_hostname=self._host
            )
        credentials = pika.credentials.PlainCredentials(username=self._username, password=self._password)
        connection_params = pika.ConnectionParameters(
            host=self._host,
            port=self._port,
            credentials=credentials,
            ssl_options=ssl_options
        )
        return pika.BlockingConnection(connection_params)

    @property
    def connection(self) -> pika.BlockingConnection:
        if not self._connection:
            try:
                self._connection = self._get_connection()
            except Exception as exc:
                logger.error("Unable to connect to the RabbitMQ: '%s'", str(exc))

        return self._connection

    @property
    def channel(self) -> pika.adapters.blocking_connection.BlockingChannel:
        if self._channel and not self._channel.is_open:
            self.disconnect()

        if not self._channel:
            try:
                self._channel = self.connection.channel()
            except Exception as exc:
                logger.error("Unable to get channel from connection, reconnecting (%s).", exc)
                self.disconnect()

        return self._channel

    def disconnect(self):
        if self._channel:
            try:
                self._channel.close()
            except Exception:
                logger.error("Unable to close channel.")
            finally:
                self._channel = None

        if self._connection:
            try:
                self._connection.close()
            except Exception:
                logger.error("Unable to close connection.")
            finally:
                self._connection = None

    def publish(self, exchange, routing_key, body):
        channel = self.channel
        if channel:
            try:
                channel.basic_publish(
                    exchange=exchange,
                    routing_key=routing_key,
                    body=body,
                )
            except Exception as exc:
                logger.error("Unable to send data to '%s', error is '%s'.", self._host, exc)
                self.disconnect()
        else:
            logger.error("Unable to send data to '%s', no channel.", self._host)
