from dataclasses import dataclass


@dataclass
class OrientationHelpersState:
    engine_helper: bool = False
    bank_helper: bool = False
    pitch_helper: bool = False
    yaw_helper: bool = False

    @classmethod
    def from_byte(cls, data):
        parameters = {}
        for parameter in ("yaw_helper", "pitch_helper", "bank_helper", "engine_helper",):
            parameters[parameter] = bool(data & 0b1)
            data >>= 1
        return cls(**parameters)

    def to_byte(self):
        result = 0
        for state in (self.engine_helper, self.bank_helper, self.pitch_helper, self.yaw_helper,):
            result |= 1 if state else 0
            result <<= 1
        result >>= 1
        return result


@dataclass
class AirplaneControlsState:
    bank: float = None
    pitch: float = None
    yaw: float = None
    flaps: float = None
    engine_power: float = None
    engine_on: bool = None
    engine_safe: bool = None
    orientation_helpers: int = 0
