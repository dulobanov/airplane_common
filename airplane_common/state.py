from dataclasses import dataclass, field
from time import time

from i2c_peripherals.devices.voltmeter.definitions import Voltmeter, VoltmeterMeasurementResult
from i2c_peripherals.utils.dataclass import AsDictMixin, DictInit, FlattenMixin

TIMESTAMP = "timestamp"


@dataclass(frozen=True)
class BaseInformation(FlattenMixin, metaclass=DictInit):
    value: float


@dataclass(frozen=True)
class Engine(FlattenMixin, metaclass=DictInit):
    safe_mode: bool
    engine_on: bool
    power: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class FlightControls(FlattenMixin, metaclass=DictInit):
    aileron_left: BaseInformation
    aileron_right: BaseInformation
    flap_left: BaseInformation
    flap_right: BaseInformation
    elevator: BaseInformation
    rudder: BaseInformation
    engine: Engine
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class AirplaneAcceleration(FlattenMixin, metaclass=DictInit):
    speed: float  # m/s^2
    shift: float  # m/s^2
    vertical: float  # m/s^2
    length: float  # absolute acceleration vector length
    # linear is acceleration that is applied due to movement
    linear_speed: float  # m/s^2
    linear_shift: float  # m/s^2
    linear_vertical: float  # m/s^2
    linear_length: float  # absolute linear acceleration vector length
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class AirplaneOrientation(FlattenMixin, metaclass=DictInit):
    bank_speed: float  # deg/s
    pitch_speed: float  # deg/s
    yaw_speed: float  # deg/s
    bank_angle: float  # deg
    pitch_angle: float  # deg
    yaw_angle: float  # deg
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class AirplaneGravityVector(FlattenMixin, metaclass=DictInit):
    speed: float
    shift: float
    vertical: float
    length: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class CPU(FlattenMixin, metaclass=DictInit):
    cpu_temperature: float
    cpu_la1: float
    cpu_la5: float
    cpu_la15: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class AccelerationState(AsDictMixin, metaclass=DictInit):
    linear_speed_acceleration: float
    linear_shift_acceleration: float
    linear_vertical_acceleration: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class BarometerState(FlattenMixin, metaclass=DictInit):
    pressure: float
    air_density: float
    temperature: float
    QNH: float
    QNH_speed: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class GyroscopeState(AsDictMixin, metaclass=DictInit):
    bank_angle: float
    pitch_angle: float
    yaw_angle: float
    bank_speed: float
    pitch_speed: float
    yaw_speed: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class AirSpeedState(FlattenMixin, metaclass=DictInit):
    speed: float
    acceleration: float
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class VoltmeterState(FlattenMixin, metaclass=DictInit):
    vcc: float
    battery: float
    timestamp: float = field(default_factory=time)

    @classmethod
    def from_voltmeter_measurement_data(cls, voltmeter_raw_data):
        values = {key: None for key in cls.__annotations__.keys()}
        voltmeter_data = Voltmeter(voltmeter_raw_data)
        values["timestamp"] = voltmeter_data.timestamp
        for measurement in voltmeter_data.measurements:
            measurement = VoltmeterMeasurementResult(measurement)
            if measurement.name in values:
                values[measurement.name] = measurement.value
        return cls(**values)


@dataclass(frozen=True)
class PIDState(FlattenMixin, metaclass=DictInit):
    kp: float
    ki: float
    kd: float
    expected: float
    current: float
    value: float
    error: float
    p: float
    i: float
    d: float
    tags: dict
    timestamp: float = field(default_factory=time)


@dataclass(frozen=True)
class State(FlattenMixin, metaclass=DictInit):
    barometer: BarometerState = None
    voltmeter: VoltmeterState = None
    flight_controls: FlightControls = None
    gyroscope: GyroscopeState = None
    accelerometer: AccelerationState = None
    pito: AirSpeedState = None
    timestamp: float = field(default_factory=time)
